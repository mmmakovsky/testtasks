import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class SumOfNumbers {
    public static void main(String[] args) {
        int[] numbers = {1, 4, 1, 3, 4, 8, 7, 9};
        System.out.println(Arrays.toString(sumsOfNumbers(numbers)));
    }

    private static Integer[] sumsOfNumbers(int[] numbers) {
        Set<Integer> sums = new TreeSet<>();
        for (int i = 0; i < numbers.length; i++) {
            for (int j = i + 1; j < numbers.length; j++) {
                sums.add(numbers[i] + numbers[j]);
            }
        }
        return sums.toArray(new Integer[sums.size()]);
    }
}
