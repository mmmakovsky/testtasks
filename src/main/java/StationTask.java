import java.util.*;

public class StationTask {

    private static List<Station> STATION_LIST = Arrays.asList(
            new Station("МОСКВА"),
            new Station("МОЖГА"),
            new Station("МОЗДОК"),
            new Station("САНКТ-ПЕТЕРБУРГ"),
            new Station("САМАРА"));

    private StationTask(List<Station> stationList) {
        stationList.sort(Comparator.comparing(Station::getName));
    }

    public static void main(String[] args) {
        StationTask task = new StationTask(STATION_LIST);
        System.out.println(task.getStationsByTwoFirstLetters("МО"));
        System.out.println(task.getStationsByTwoFirstLetters("СА"));
    }

    private Collection<Station> getStationsByTwoFirstLetters(String prefix) {
        List<Station> result = new ArrayList<>();
        for (Station station : STATION_LIST) {
            if (station.getName().substring(0, 2).compareTo(prefix) == 0) {
                result.add(station);
            } else if (station.getName().substring(0, 2).compareTo(prefix) < 0) {
                return result;
            }
        }
        return result;
    }

    private static class Station {

        private String name;

        public Station(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "Station{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }
}