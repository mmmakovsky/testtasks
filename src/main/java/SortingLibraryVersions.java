import java.util.Arrays;
import java.util.Comparator;

public class SortingLibraryVersions {
    public static void main(String[] args) {
        String[] versions = {"1.23.2","2.2.5", "3.3.12", "1.111.2", "2.2.1", "5.3.12", "4.23.44", "9.2.1", "5.99.12"};
        sortVersions(versions);
    }

    private static String[] sortVersions(String[] versions) {
        Comparator<String> versionComparator = (o1, o2) -> {
            String[] firstVersionStr = o1.split("\\.");
            String[] secondVersionStr = o2.split("\\.");
            int[] firstInt = new int[3];
            int[] secondInt = new int[3];
            for (int i = 0; i < 3; i++) {
                firstInt[i] = Integer.parseInt(firstVersionStr[i]);
                secondInt[i] = Integer.parseInt(secondVersionStr[i]);
            }
            if (firstInt[0] > secondInt[0] || firstInt[0] == secondInt[0] && firstInt[1] > secondInt[1] ||
                    firstInt[0] == secondInt[0] && firstInt[1] == secondInt[1] && firstInt[2] > secondInt[2]) {
                return 1;
            } else if (firstInt[0] == secondInt[0] && firstInt[1] == secondInt[1] && firstInt[2] == secondInt[2]) {
                return 0;
            } else {
                return -1;
            }
        };
        Arrays.sort(versions, versionComparator);
        for (String s : versions) {
            System.out.println(s);
        }
        return versions;
    }
}
